package ru.ckateptb.core.storage.sql.query.column;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.Validate;
import ru.ckateptb.core.storage.sql.query.QueryBuilder;

@RequiredArgsConstructor
public class ColumnBuilder implements QueryBuilder {
    private final String name;
    private final ColumnType type;
    private boolean primary;
    private boolean autoincrement;
    private boolean unique;
    private boolean notNull;
    private Object defaultValue;

    public ColumnBuilder primary() {
        this.primary = true;
        return this;
    }

    public ColumnBuilder autoincrement() {
        this.autoincrement = true;
        return this;
    }

    public ColumnBuilder unique() {
        this.unique = true;
        return this;
    }

    public ColumnBuilder notNull() {
        this.notNull = true;
        return this;
    }

    public ColumnBuilder defaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!name.trim().isEmpty());
        StringBuilder builder = new StringBuilder(name);
        builder.append(" ");
        builder.append(type.name());
        if (primary) {
            builder.append(" PRIMARY KEY");
        }
        if (autoincrement) {
            builder.append(" AUTOINCREMENT");
        }
        if (unique) {
            builder.append(" UNIQUE");
        }
        if (notNull) {
            builder.append(" NOT NULL");
        }
        if (defaultValue != null) {
            builder.append(String.format(" DEFAULT %s", defaultValue));
        }
        return builder.toString();
    }
}
