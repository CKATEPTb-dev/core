package ru.ckateptb.core.storage.sql;

import java.sql.ResultSet;

public interface QueryListener {
    void onInteract(ResultSet resSet) throws Exception;
}
