package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;

public class SelectQueryBuilder implements QueryBuilder {
    private final String table;
    private String[] columns;
    private String where;
    private String[] order;
    private int limit;
    private int offset;
    private boolean distinct;
    private String group;
    private String having;

    public SelectQueryBuilder(String table) {
        this.table = table;
    }

    public SelectQueryBuilder columns(String... columns) {
        this.columns = columns;
        return this;
    }

    public SelectQueryBuilder all() {
        return columns("*");
    }

    public SelectQueryBuilder where(String where) {
        this.where = where;
        return this;
    }

    public SelectQueryBuilder order(String... order) {
        this.order = order;
        return this;
    }

    public SelectQueryBuilder limit(int limit) {
        this.limit = limit;
        return this;
    }

    public SelectQueryBuilder offset(int offset) {
        this.offset = offset;
        return this;
    }

    public SelectQueryBuilder distinct() {
        this.distinct = true;
        return this;
    }

    public SelectQueryBuilder group(String group) {
        this.group = group;
        return this;
    }

    public SelectQueryBuilder having(String having) {
        this.having = having;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!table.trim().isEmpty());
        Validate.notEmpty(columns);
        StringBuilder builder = new StringBuilder("select ");
        if (distinct) builder.append("distinct ");
        builder.append(String.join(", ", columns));
        builder.append(" from ");
        builder.append(table);
        if (where != null && !where.isEmpty()) {
            builder.append(" where ");
            builder.append(where);
        }
        if (order != null) {
            builder.append(" order by ");
            builder.append(String.join(", ", order));
        }
        if (limit > 0) {
            builder.append(" limit ");
            builder.append(limit);
        }
        if (offset > 0) {
            builder.append(" offset ");
            builder.append(offset);
        }
        if (group != null && group.trim().length() > 0) {
            builder.append(" group by ");
            builder.append(group);
        }
        if (having != null && having.trim().length() > 0) {
            builder.append(" having ");
            builder.append(having);
        }
        builder.append(";");
        return builder.toString();
    }
}
