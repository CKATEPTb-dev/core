package ru.ckateptb.core.storage.sql;

import lombok.SneakyThrows;
import ru.ckateptb.core.storage.Storage;

import java.sql.Connection;
import java.sql.PreparedStatement;

public abstract class SQLiteStorage implements Storage, AutoCloseable {
    protected Connection connection;

    public SQLiteStorage() {
        connect();
    }

    @SneakyThrows
    public void connect() {
        connection = DataSourceHelper.createDefaultSQLiteHikariDataSource(file()).getConnection();
    }

    @SneakyThrows
    public void disconnect() {
        if (!connection.isClosed()) {
            connection.close();
        }
    }

    public void execute(final String query, final Object... args) {
        prepare(ps -> {
            int i = 1;
            for (Object x : args) {
                ps.setObject(i++, x);
            }
            ps.execute();
        }, query);
    }

    public void execute(final QueryListener ql, final String query, Object... args) {
        prepare(ps -> {
            int i = 1;
            for (Object x : args) {
                ps.setObject(i++, x);
            }
            ql.onInteract(ps.executeQuery());
        }, query);
    }

    @SneakyThrows
    public void prepare(final ExecuteListener el, final String query) {
        if (connection.isClosed()) {
            connect();
        }
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            el.onInteract(ps);
        }
    }

    @Override
    public void close() {
        this.disconnect();
    }
}