package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;

public class DeleteQueryBuilder implements QueryBuilder {
    private final String form;
    private String where;

    public DeleteQueryBuilder(String from) {
        this.form = from;
    }

    public DeleteQueryBuilder where(String where) {
        this.where = where;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!form.trim().isEmpty());
        StringBuilder builder = new StringBuilder("delete from ");
        builder.append(form);
        if(where != null && !where.trim().isEmpty()) {
            builder.append(" ");
            builder.append("where ");
            builder.append(where);
        }
        builder.append(";");
        return builder.toString();
    }
}
