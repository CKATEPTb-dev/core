package ru.ckateptb.core.storage.sql.query.column;

public enum ColumnType {
    NULL,
    INTEGER,
    REAL,
    TEXT,
    BLOB
}
