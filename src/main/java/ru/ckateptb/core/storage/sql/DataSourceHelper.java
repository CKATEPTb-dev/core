package ru.ckateptb.core.storage.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.nio.file.Path;

public class DataSourceHelper {
    @SneakyThrows
    public static HikariDataSource createDefaultSQLiteHikariDataSource(Path path) {
        FileUtils.forceMkdirParent(path.toFile());
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.sqlite.JDBC");
        config.setJdbcUrl("jdbc:sqlite:" + path);
        config.setConnectionTimeout(0);
        return new HikariDataSource(config);
    }
}
