package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;
import ru.ckateptb.core.storage.sql.query.column.ColumnBuilder;

import java.util.Arrays;

public class CreateQueryBuilder implements QueryBuilder {
    private final String table;
    private final ColumnBuilder[] columns;
    private boolean ifNoExists;

    public CreateQueryBuilder(String table, ColumnBuilder... columns) {
        this.table = table;
        this.columns = columns;
    }

    public CreateQueryBuilder ifNoExists() {
        this.ifNoExists = true;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!table.trim().isEmpty());
        StringBuilder builder = new StringBuilder("create table ");
        if (ifNoExists) builder.append("if not exists ");
        builder.append(table);
        builder.append(" (");
        builder.append(String.join(", ", Arrays.stream(columns).map(ColumnBuilder::build).toArray(String[]::new)));
        builder.append(");");
        return builder.toString();
    }
}
