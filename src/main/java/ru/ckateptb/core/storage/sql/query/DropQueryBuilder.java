package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;

public class DropQueryBuilder implements QueryBuilder {
    private final String table;
    private boolean ifExists;

    public DropQueryBuilder(String table) {
        this.table = table;
    }

    public DropQueryBuilder ifExists() {
        this.ifExists = true;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!table.trim().isEmpty());
        StringBuilder builder = new StringBuilder("drop table ");
        if (ifExists) builder.append("if exists ");
        builder.append(table);
        builder.append(";");
        return builder.toString();
    }
}
