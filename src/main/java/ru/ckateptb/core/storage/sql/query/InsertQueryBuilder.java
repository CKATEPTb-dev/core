package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;

import java.util.LinkedHashMap;
import java.util.Map;

public class InsertQueryBuilder implements QueryBuilder {
    private final String table;
    private final Map<String, String> map = new LinkedHashMap<>();

    public InsertQueryBuilder(String table) {
        this.table = table;
    }

    public InsertQueryBuilder set(String column, String value) {
        map.put(column, value);
        return this;
    }

    public InsertQueryBuilder set(String column) {
        return set(column, "?");
    }

    @Override
    public String build() {
        Validate.isTrue(!table.trim().isEmpty());
        Validate.notEmpty(map);
        String query = "insert into %s (%s) values (%s)";
        return String.format(query, table, String.join(", ", map.keySet()), String.join(", ", map.values()));
    }
}
