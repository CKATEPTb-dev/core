package ru.ckateptb.core.storage.sql;

import java.sql.PreparedStatement;

public interface ExecuteListener {
    void onInteract(PreparedStatement resSet) throws Exception;
}