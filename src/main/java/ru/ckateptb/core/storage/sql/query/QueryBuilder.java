package ru.ckateptb.core.storage.sql.query;

public interface QueryBuilder {
    String build();
}
