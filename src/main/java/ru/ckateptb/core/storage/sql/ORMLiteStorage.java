package ru.ckateptb.core.storage.sql;

import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.SerializableType;
import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.SneakyThrows;
import ru.ckateptb.core.storage.Storage;

import java.lang.reflect.Field;
import java.util.Collection;

@Getter
public abstract class ORMLiteStorage implements Storage, AutoCloseable {
    protected final ConnectionSource connection;

    @SneakyThrows
    public ORMLiteStorage() {
        HikariDataSource dataSource = DataSourceHelper.createDefaultSQLiteHikariDataSource(file());
        this.connection = new DataSourceConnectionSource(dataSource, dataSource.getJdbcUrl());
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
    public static class SerializableCollectionsType extends SerializableType {
        private static SerializableType singleton;
        public SerializableCollectionsType() {
            super(SqlType.SERIALIZABLE, new Class<?>[0]);
        }
        public static SerializableType getSingleton() {
            if (singleton == null) {
                singleton = SerializableType.getSingleton();
            }
            return singleton;
        }
        @Override
        public boolean isValidForField(Field field) {
            return Collection.class.isAssignableFrom(field.getType());
        }
    }
}