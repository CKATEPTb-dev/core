package ru.ckateptb.core.storage.sql.query;

import org.apache.commons.lang3.Validate;

import java.util.HashMap;
import java.util.Map;

public class UpdateQueryBuilder implements QueryBuilder {
    private final String table;
    private final Map<String, String> values = new HashMap<>();
    private String where;
    private String[] order;
    private int limit;
    private int offset;

    public UpdateQueryBuilder(String table) {
        this.table = table;
    }

    public UpdateQueryBuilder set(String column, String value) {
        values.put(column, value);
        return this;
    }

    public UpdateQueryBuilder set(String column) {
        return set(column, "?");
    }

    public UpdateQueryBuilder where(String where) {
        this.where = where;
        return this;
    }

    public UpdateQueryBuilder order(String... order) {
        this.order = order;
        return this;
    }

    public UpdateQueryBuilder limit(int limit) {
        this.limit = limit;
        return this;
    }

    public UpdateQueryBuilder offset(int offset) {
        this.offset = offset;
        return this;
    }

    @Override
    public String build() {
        Validate.isTrue(!table.trim().isEmpty());
        Validate.notEmpty(values);
        StringBuilder builder = new StringBuilder("update ");
        builder.append(table);
        builder.append(" set ");
        builder.append(String.join(", ", values.keySet().stream().map(value -> value + " = " + values.get(value)).toArray(String[]::new)));
        if (where != null && !where.isEmpty()) {
            builder.append(" where ");
            builder.append(where);
        }
        if (order != null) {
            builder.append(" order by ");
            builder.append(String.join(", ", order));
        }
        if (limit > 0) {
            builder.append(" limit ");
            builder.append(limit);
        }
        if (offset > 0) {
            builder.append(" offset ");
            builder.append(offset);
        }
        builder.append(";");
        return builder.toString();
    }
}
