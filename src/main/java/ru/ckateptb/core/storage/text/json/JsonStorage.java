package ru.ckateptb.core.storage.text.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import ru.ckateptb.core.storage.text.TextStorage;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class JsonStorage implements TextStorage {
    //    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @PostConstruct
    public void init() {
        load();
    }

    @SneakyThrows
    public void load() {
        Path path = file();
        if (!Files.exists(path)) {
            save();
            return;
        }
        File file = path.toFile();
        Class<? extends JsonStorage> clazz = this.getClass();
//        Object obj = OBJECT_MAPPER.readValue(file, clazz);
        try (Reader reader = new FileReader(file)) {
            Object obj = GSON.fromJson(reader, clazz);
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                field.set(this, field.get(obj));
            }
        }
    }

    @SneakyThrows
    public void save() {
        Path path = file();
        Path parent = path.getParent();
        if (parent != null && !Files.isDirectory(parent)) {
            Files.createDirectories(parent);
        }
        File file = path.toFile();
        try (Writer writer = new FileWriter(file)) {
            GSON.toJson(this, writer);
        }
//        OBJECT_MAPPER.writeValue(file, this);
    }
}
