package ru.ckateptb.core.storage.text;


import ru.ckateptb.core.storage.Storage;

public interface TextStorage extends Storage {
    void load();

    void save();
}
