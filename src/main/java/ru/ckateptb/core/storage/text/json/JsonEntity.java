package ru.ckateptb.core.storage.text.json;

import lombok.SneakyThrows;

public abstract class JsonEntity {
//    private static final ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    @Override
    public String toString() {
        return JsonStorage.GSON.toJson(this);
    }
}
