package ru.ckateptb.core.storage;

import java.nio.file.Path;

public interface Storage {
    Path file();
}
