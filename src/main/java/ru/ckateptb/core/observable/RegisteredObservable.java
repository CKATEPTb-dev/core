package ru.ckateptb.core.observable;

import lombok.NoArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@NoArgsConstructor
public abstract class RegisteredObservable<T extends Observer> implements IObservable<T> {

    @Scheduled(fixedDelay = 0, initialDelay = 0)
    public void init() {
        this.update();
    }
}