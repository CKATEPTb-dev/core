package ru.ckateptb.core.observable;

public interface Observer {
    default void update() {
    }
}
