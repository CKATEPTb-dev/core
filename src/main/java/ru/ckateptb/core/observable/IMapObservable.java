package ru.ckateptb.core.observable;

public interface IMapObservable<K, V extends Observer> extends IObservable<V> {
    void addObserver(K key, V o);

    void deleteObserver(K key);

    V get(K key);

    K get(V o);
}
