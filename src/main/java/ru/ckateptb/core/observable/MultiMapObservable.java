package ru.ckateptb.core.observable;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public class MultiMapObservable<K, V extends Observer> extends RegisteredObservable<V> implements IMapObservable<K, V> {
    private final MultiValuedMap<K, V> instances = new ArrayListValuedHashMap<>();

    @Override
    public synchronized void addObserver(K key, V o) {
        Validate.notNull(key);
        Validate.notNull(o);
        instances.put(key, o);
    }

    @Override
    public synchronized void deleteObserver(K key) {
        deleteObserver(key, null);
    }

    @Override
    public void addObserver(V o) {
        addObserver(null, o);
    }

    @Override
    public synchronized void deleteObserver(V o) {
        deleteObserver(null, o);
    }

    public synchronized void deleteObserver(K key, V o) {
        Validate.notNull(key);
        Validate.notNull(o);
        instances.removeMapping(key, o);
    }

    @Override
    public synchronized void clear() {
        instances.clear();
    }

    @Override
    public synchronized int size() {
        return instances.size();
    }

    @Override
    public synchronized void update() {
        get().forEach(V::update);
    }

    @Override
    public synchronized Collection<V> get() {
        return new CopyOnWriteArrayList<>(instances.values());
    }

    public synchronized Collection<V> getCollection(K key) {
        return instances.get(key);
    }


    @Override
    public V get(K key) {
        return Validate.notNull(null);
    }

    @Override
    public K get(V o) {
        return Validate.notNull(null);
    }


}
