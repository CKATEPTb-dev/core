package ru.ckateptb.core.observable;



import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Observable<T extends Observer> extends RegisteredObservable<T> {
    private final Vector<T> instances = new Vector<>();

    @Override
    public void addObserver(T o) {
        Validate.notNull(o);
        instances.add(o);
    }

    @Override
    public void deleteObserver(T o) {
        Validate.notNull(o);
        instances.remove(o);
    }

    @Override
    public void clear() {
        instances.clear();
    }

    @Override
    public int size() {
        return instances.size();
    }

    @Override
    public void update() {
        get().forEach(T::update);
    }

    @Override
    public List<T> get() {
        return new ArrayList<>(instances);
    }
}
