package ru.ckateptb.core.observable;

import java.util.Collection;

public interface IObservable<T extends Observer> {

    void addObserver(T o);

    void deleteObserver(T o);

    void clear();

    int size();

    void update();

    Collection<T> get();

}
