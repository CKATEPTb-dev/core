package ru.ckateptb.core.observable;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapObservable<K, V extends Observer> extends RegisteredObservable<V> implements IMapObservable<K, V> {
    private final BidiMap<K, V> instances = new DualHashBidiMap<>();

    @Override
    public synchronized void addObserver(V o) {
        addObserver(null, o);
    }

    @Override
    public synchronized void addObserver(K key, V o) {
        Validate.notNull(o);
        instances.put(key, o);
    }

    @Override
    public synchronized void deleteObserver(K key) {
        instances.remove(key);
    }

    @Override
    public synchronized void deleteObserver(V o) {
        Validate.notNull(o);
        instances.removeValue(o);
    }

    @Override
    public synchronized void clear() {
        instances.clear();
    }

    @Override
    public synchronized int size() {
        return instances.size();
    }

    @Override
    public synchronized void update() {
        get().forEach(V::update);
    }

    @Override
    public synchronized Collection<V> get() {
        return new CopyOnWriteArrayList<>(instances.values());
    }

    @Override
    public synchronized V get(K key) {
        return instances.get(key);
    }

    @Override
    public synchronized K get(V o) {
        return instances.getKey(o);
    }
}
