package ru.ckateptb.core.utils.date;

import com.j256.ormlite.dao.DaoManager;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class DateRange implements Iterable<LocalDate> {
    @Getter
    private final LocalDate startDate, endDate;
    private final long start, end;

    public DateRange(LocalDate startDate, LocalDate endDate) {
        Validate.notNull(startDate);
        Validate.notNull(endDate);
        this.startDate = startDate;
        this.endDate = endDate;
        long start = this.startDate.toEpochDay();
        long end = this.endDate.toEpochDay();
        this.start = Math.min(start, end);
        this.end = LocalDate.ofEpochDay(Math.max(start, end)).plusDays(1).toEpochDay();
    }

    @Override
    public Iterator<LocalDate> iterator() {
        return stream().iterator();
    }

    public Stream<LocalDate> stream() {
        return LongStream.range(start, end).mapToObj(LocalDate::ofEpochDay);
    }
}