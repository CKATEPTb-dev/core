package ru.ckateptb.core.spring;

import lombok.Getter;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.support.GenericApplicationContext;

public class LazySpring {
    @Getter
    private static final GenericApplicationContext context = new GenericApplicationContext();
    @Getter
    private static final ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(context);

    public static void initialize(Package pack) {
        scanner.addIncludeFilter((metadataReader, metadataReaderFactory) -> false);
        scanner.scan(pack.getName());
        context.refresh();
    }

    public static <T> T get(Class<T> tClass) {
        return context.getBean(tClass);
    }

}
